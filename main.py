import uasyncio as asyncio
from app import app_start

loop = asyncio.get_event_loop()
loop.create_task(app_start())

try:
    loop.run_forever()
finally:
    loop.close()
