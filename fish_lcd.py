
from machine import I2C, Pin
from esp8266_i2c_lcd import I2cLcd

SDA_PIN = 4
SCL_PIN = 5

i2c= I2C(scl=Pin(SCL_PIN), sda=Pin(SDA_PIN), freq=400000)


class FishLCD():
    def __init__(self):
        self.lcd = I2cLcd(i2c, 0x27, 2, 16)


    def show(self, message):
        self.lcd.putstr(message)
    
    def clean(self):
        self.lcd.clear()
