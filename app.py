from micropyserver import MicroPyServer
from heater import Heater
from led import LEDController

import ujson
import ure
import machine
import micropython
import gc
import urequests

water_heater = Heater()


led = {
    "white": LEDController(12),
    "red": LEDController(14),
    "green": LEDController(13),
    "blue": LEDController(15),
}

class HttpHelper:
    """
        Простой парсер GET запросов
    """
    def __init__(self):
        pass

    def parse(self, request):
        '''
            парсер строки ответа сервера
        '''
        lines = request.split("\r\n")

        result = {
            'lines': lines,
            'method': ure.search("^([A-Z]+)", lines[0]).group(1),
            'path': ure.search(
                "^[A-Z]+\\s+(/[-a-zA-Z0-9_.]*)", lines[0]
            ).group(1),
        }

        param_split = ure.sub("\/([a-z]+_?)+?\?", '', lines[0].split(" ")[1])
        result['params'] = self.get_params(param_split.split("&"))

        return result

    def get_params(self, params_as_array):
        '''
            создает словарь для query параметров
        '''
        params = {}

        for element in params_as_array:
            splited = element.split("=")

            params[splited[0]] = splited[1]

        return params

    def send(self, **kwargs):
        '''
            Отправляем ответ
        '''
        server.send(
            ujson.dumps(kwargs),
            content_type="Content-Type: application/json",
            extra_headers=["Access-Control-Allow-Origin: *"]
        )
        gc.collect()

http_helper = HttpHelper()


def led_controller(request):
    '''
        Контролле LED матриц
    '''
    try:
        data = http_helper.parse(request)

        impulse = int(data['params']['impulse'])
        level = int(data['params']['level'])
        ledName = data['params']['ledName']

        led[ledName].soft_switching(level)
        led[ledName].set_led_impulse(impulse)

        http_helper.send(
            success=1,
            set_level=level,
            set_impulse=impulse,
            led_name=ledName,
        )

    except Exception as e:
        print(e)
        http_helper.send(success=0, error=e)

def get_led_info(request):
    '''
        Возвращаем данные матриц освещения
    '''
    try:
        data = http_helper.parse(request)

        ledName = data['params']['ledName']
        impulse = led[ledName].get_led_impulse()
        level = led[ledName].get_led_level()

        http_helper.send(
            success=1,
            level=level,
            impulse=impulse,
            led_name=ledName,
        )

    except Exception as e:
        print(e)
        http_helper.send(success=0, error=e)


def get_water_temperature_C(request):
    '''
        Отдает температуру в Цельсиях
    '''
    try:
        water_heater.get_water_tmp_C()

        http_helper.send(
            success=1,
            water_temperature_c=water_heater.water_tmp
        )

    except Exception as e:
        print(e)
        http_helper.send(success=0, error=e)

def synch_date_time(request):
    '''
        Синхронизируем время
    '''
    try:
        response = urequests.get(
            "http://worldtimeapi.org/api/timezone/Europe/Moscow"
        )
        parse_data = response.json()['datetime'].split('T')
        day_of_year = response.json()['day_of_year']
        day_of_week = response.json()['day_of_week']
        date = parse_data[0]
        time = parse_data[1].split('.')[0]


        syncTimeArray = date.split('-')
        syncTimeArray.append(day_of_week)
        syncTimeArray = syncTimeArray + time.split(':')
        syncTimeArray = [ int(x) for x in syncTimeArray ]
        syncTimeArray.append(day_of_year)


        # (year, month, day, weekday, hours, minutes, seconds, subseconds)
        machine.RTC().datetime(syncTimeArray)

        print(machine.RTC().datetime())
        print(syncTimeArray)

        response.close()

        http_helper.send(
            success=1,
            time=time,
            date=date
        )
        gc.collect()
    except Exception as e:
        print(e)
        http_helper.send(success=0, error=e)

def get_date_and_time(request, raw_return=False):
    '''
        Возвращает дату и время в Москве
    '''
    try:
        date_time = machine.RTC().datetime()

        micropython.mem_info()

        day = date_time[2]
        month = date_time[1]
        year = date_time[0]

        hours = date_time[4]
        minutes =  date_time[5]
        seconds = date_time[6]

        # Форматируем дату
        if day >= 1 and day <= 9:
            day = '{}{}'.format(0, day)

        if month >= 1 and month <= 9:
            month = '{}{}'.format(0, month)

        # Форматируем время
        if hours >= 1 and hours <= 9:
            hours = '{}{}'.format(0, hours)

        if minutes >= 1 and minutes <= 9:
            minutes = '{}{}'.format(0, minutes)

        if seconds >= 1 and seconds <= 9:
            seconds = '{}{}'.format(0, seconds)

        time = '{}:{}:{}'.format(hours, minutes, seconds)
        date = '{}-{}-{}'.format(day, month, year)
        
        if not raw_return:
            http_helper.send(
                success=1,
                time=time,
                date=date
            )
        else:
            return {
                "success": 1,
                "time": time,
                "date": date
            }
        gc.collect()
    except Exception as e:
        print(e)
        http_helper.send(success=0, error=e)



server = MicroPyServer()
''' add request handler '''
server.add_route("/get_led_info", get_led_info)
server.add_route("/led_controller", led_controller)
server.add_route("/get_water_tmp", get_water_temperature_C)
server.add_route("/get_date_and_time", get_date_and_time)
server.add_route("/synch_date_time", synch_date_time)

def app_start():
    server.start()