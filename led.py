from machine import Pin, PWM
import time

class LEDController():
    def __init__(self, pin, frequency=4000):
        self.rawPin = pin
        self.pwmPin = PWM(Pin(pin), frequency)

    def set_led_level(self, level):
        self.pwmPin.duty(level)
    
    def set_led_impulse(self, impulse_p_s):
        self.pwmPin.freq(impulse_p_s)
    
    def get_led_level(self):
        return self.pwmPin.duty()

    def get_led_impulse(self):
        return self.pwmPin.freq()

    def led_off(self):
        self.pwmPin.duty(1)

    def fading(self):
        level = self.get_led_level()

        for l in reversed(range(0, level)):
            self.set_led_level(l)
            time.sleep_ms(15)

    def riseup(self, to_level):
        level = self.get_led_level()

        for l in range(level, to_level, 1):
            self.set_led_level(l)
            time.sleep_ms(20)

    def soft_switching(self, value):
        curent_level = self.get_led_level()

        if value > curent_level:
            for higher_lvl in range(curent_level, value, 1):
                self.set_led_level(higher_lvl)
                time.sleep_ms(20)
        elif value < curent_level:
            for lower_lvl in range(curent_level, value, -1):
                self.set_led_level(lower_lvl)
                time.sleep_ms(20)
        else:
            pass
